const express = require("express");
const router = express.Router();
const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");
const keys = require("../config/keys");

//Validation
const validateRegisterInput = require("../validation/register");
const validateLoginInput = require("../validation/login");

//Model
const User = require("../models/User");

//Register route starts

//Create
router.post("/register", (req, res) => {
  //Form validation
  const { errors, isValid } = validateRegisterInput(req.body);

  if (!isValid) {
    return res.status(400).json(errors);
  }

  User.findOne({ email: req.body.email }).then((user) => {
    if (user) {
      return res.status(400).json({ email: "Email already in use" });
    } else {
      const newUser = new User({
        username: req.body.username,
        password: req.body.password,
        email: req.body.email,
        saved_events: [],
        is_admin: false,
        is_banned: false,
      });

      // Hash password
      bcrypt.genSalt(10, (err, salt) => {
        bcrypt.hash(newUser.password, salt, (err, hash) => {
          if (err) throw err;
          newUser.password = hash;
          newUser
            .save()
            .then((user) => res.json(user))
            .catch((err) => console.log(err));
        });
      });
    }
  });
});

//Get all
// router.get("/", (req, res) => {
//   User.find().then((users) => res.json(users));
// });

//Get specific
router.get("/:id", (req, res) => {
  User.findById(req.params.id)
    .then((user) => res.json(user))
    .catch((err) => res.status(404).json({ err: "Not found" }));
});

//Add event
router.put("/add_event/:id", (req, res) => {
  User.findByIdAndUpdate(
    req.params.id,
    {
      $push: {
        saved_events: {
          _id: req.body._id,
          title: req.body.title,
          start: req.body.start,
          end: req.body.end,
        },
      },
    },
    { new: true }
  )
    .then((user) => res.json(user))
    .catch((err) => res.status(404).json({ err: "Not found" }));
});

//Remove event
router.put("/remove_event/:id", (req, res) => {
  User.findByIdAndUpdate(
    req.params.id,
    {
      $pull: {
        saved_events: {
          _id: req.body._id,
        },
      },
    },
    { new: true }
  )
    .then((user) => res.json(user))
    .catch((err) => res.status(404).json({ err: "Not found" }));
});

// //Delete
// router.delete("/:id", (req, res) => {
//   User.findById(req.params.id)
//     .then((user) => user.remove().then(() => res.json({ msg: "User deleted" })))
//     .catch((err) => res.status(404).json({ err: "Not found" }));
// });

//Login route starts

router.post("/login", (req, res) => {
  // Form validation
  const { errors, isValid } = validateLoginInput(req.body);
  if (!isValid) {
    return res.status(400).json(errors);
  }
  const email = req.body.email;
  const password = req.body.password;

  User.findOne({ email }).then((user) => {
    // Check if user exists
    if (!user) {
      return res.status(404).json({ emailnotfound: "Email not found" });
    }
    // Check password
    bcrypt.compare(password, user.password).then((isMatch) => {
      if (isMatch) {
        const payload = {
          id: user._id,
          username: user.username,
          is_admin: user.is_admin,
        };
        jwt.sign(
          payload,
          keys.secretOrKey,
          {
            expiresIn: 31556926,
          },
          (err, token) => {
            res.json({
              success: true,
              token: "Bearer " + token,
            });
          }
        );
      } else {
        return res
          .status(400)
          .json({ passwordincorrect: "Password incorrect" });
      }
    });
  });
});

module.exports = router;
