const express = require("express");
const router = express.Router();

//Model
const StoreEvent = require("../../models/StoreEvent");

// Create;
router.post("/", (req, res) => {
  const newStoreEvent = new StoreEvent({
    title: req.body.title,
    game: req.body.game,
    desc: req.body.desc,
    start: req.body.start,
    end: req.body.end,
    recursEvery: req.body.recursEvery,
    allDay: req.body.allDay,
    slots: req.body.slots,
    reservedUsers: [],
    isCanceled: false,
  });

  newStoreEvent.save().then((storeEvent) => res.json(storeEvent));
});

//Get all
router.get("/", (req, res) => {
  StoreEvent.find().then((storeEvents) => res.json(storeEvents));
});

// Get specific
router.get("/:id", (req, res) => {
  StoreEvent.findById(req.params.id)
    .then((storeEvent) => res.json(storeEvent))
    .catch((err) => res.status(404).json({ err: "Not found" }));
});

//Search
router.get("/search/:query", (req, res) => {
  StoreEvent.find({ title: new RegExp(".*" + req.params.query + ".*", "i") })
    .then((storeEvent) => res.json(storeEvent))
    .catch((err) => res.status(404).json({ err: "Not found!" }));
});

//Update
router.put("/update/:id", (req, res) => {
  StoreEvent.findByIdAndUpdate(
    req.params.id,
    {
      title: req.body.title,
      game: req.body.game,
      desc: req.body.desc,
      start: req.body.start,
      end: req.body.end,
      recursEvery: req.body.recursEvery,
      allDay: req.body.allDay,
      slots: req.body.slots,
      isCanceled: false,
    },
    { new: true }
  )
    .then((storeEvent) => res.json(storeEvent))
    .catch((err) => res.status(404).json({ err: "Not found" }));
});

//Reserve
router.put("/reserve/:id", (req, res) => {
  StoreEvent.findByIdAndUpdate(
    req.params.id,
    {
      $push: {
        reservedUsers: {
          _id: req.body._id,
        },
      },
    },
    { new: true }
  )
    .then((storeEvent) => res.json(storeEvent))
    .catch((err) => res.status(404).json({ err: "Not found" }));
});

// Unreserve
router.put("/cancel_reserve/:id", (req, res) => {
  StoreEvent.findByIdAndUpdate(
    req.params.id,
    {
      $pull: {
        reservedUsers: {
          _id: req.body._id,
        },
      },
    },
    { new: true }
  )
    .then((storeEvent) => res.json(storeEvent))
    .catch((err) => res.status(404).json({ err: "Not found" }));
});

//Delete
router.delete("/:id", (req, res) => {
  StoreEvent.findById(req.params.id)
    .then((storeEvent) =>
      storeEvent.remove().then(() => res.json({ msg: "Event deleted" }))
    )
    .catch((err) => res.status(404).json({ err: "Not found" }));
});

module.exports = router;
