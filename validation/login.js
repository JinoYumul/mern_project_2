const Validator = require("validator");
const isEmpty = require("is-empty");

module.exports = function validateLoginInput(data) {
  let errors = {};

  // Convert empty fields to empty string for validator functions
  data.email = !isEmpty(data.email) ? data.email : "";
  data.password = !isEmpty(data.password) ? data.password : "";

  // Email check
  if (Validator.isEmpty(data.email)) {
    errors.email = "Please enter a valid email";
  } else if (!Validator.isEmail(data.email)) {
    errors.email = "Email is invalid";
  }
  // Password check
  if (Validator.isEmpty(data.password)) {
    errors.password = "Please enter your password";
  }
  return {
    errors,
    isValid: isEmpty(errors),
  };
};
