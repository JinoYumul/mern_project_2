const mongoose = require("mongoose");
const Schema = mongoose.Schema;

//Create Schema
const StoreEventSchema = new Schema(
  {
    title: {
      type: String,
      required: true,
    },
    game: {
      type: String,
      required: true,
    },
    desc: {
      type: String,
      required: true,
    },
    start: {
      type: Date,
      required: true,
    },
    end: {
      type: Date,
      required: true,
    },
    recursEvery: {
      type: Number,
      default: undefined,
    },
    allDay: {
      type: Boolean,
      default: false,
    },
    reservedUsers: {
      type: [{ _id: String }],
    },
    isCanceled: {
      type: Boolean,
      default: false,
    },
    slots: {
      type: Number,
    },
  },
  {
    versionKey: false,
  }
);

module.exports = StoreEvent = mongoose.model("storeEvent", StoreEventSchema);
