import React from "react";
import { Input, Form, Button } from "reactstrap";

const SearchBar = (props) => {
  return (
    <Form className="form-inline my-2 w-50 justify-content-center">
      <Input
        type="search"
        name="search"
        id="exampleSearch"
        placeholder="Search"
        className="w-50"
      />
      <Button type="submit">Search</Button>
    </Form>
  );
};

export default SearchBar;
