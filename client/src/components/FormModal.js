import React, { Component } from "react";
import {
  Button,
  Modal,
  ModalHeader,
  ModalBody,
  ModalFooter,
  Form,
  FormGroup,
  Label,
  Input,
} from "reactstrap";

class FormModal extends Component {
  render() {
    const {
      isOpen,
      toggle2,
      className,
      postRecord,
      onChange,
      recurring,
    } = this.props;

    return (
      <Modal isOpen={isOpen} toggle={toggle2} className={className}>
        <ModalHeader toggle={toggle2}>Add Event</ModalHeader>
        <Form onSubmit={postRecord}>
          <ModalBody>
            <FormGroup>
              <Label for="title">Name:</Label>
              <Input
                type="text"
                name="title"
                id="title"
                placeholder="Event name"
                onChange={onChange}
              />
            </FormGroup>
            <FormGroup>
              <Label for="game">Game:</Label>
              <Input
                type="text"
                name="game"
                id="game"
                placeholder="Game"
                onChange={onChange}
              />
            </FormGroup>
            <FormGroup>
              <Label for="desc">Description:</Label>
              <Input
                type="text"
                name="desc"
                id="desc"
                placeholder="Description"
                onChange={onChange}
              />
            </FormGroup>
            <FormGroup>
              <Label for="start">Start Date:</Label>
              <Input type="date" name="start" id="start" onChange={onChange} />
            </FormGroup>
            <FormGroup>
              <Label for="end">End Date:</Label>
              <Input type="date" name="end" id="end" onChange={onChange} />
            </FormGroup>
            <FormGroup>
              <Label for="slots">Slots:</Label>
              <Input type="number" name="slots" onChange={onChange} />
            </FormGroup>
            <FormGroup check>
              <Input type="checkbox" onChange={recurring} /> Weekly?
            </FormGroup>
          </ModalBody>
          <ModalFooter>
            <Button color="primary" type="submit">
              Submit
            </Button>{" "}
            <Button color="secondary" onClick={toggle2}>
              Close
            </Button>
          </ModalFooter>
        </Form>
      </Modal>
    );
  }
}

export default FormModal;
