import React, { Component } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { registerUser } from "../actions/authActions";
import {
  Button,
  Modal,
  ModalHeader,
  ModalBody,
  ModalFooter,
  Form,
  FormGroup,
  Label,
  Input,
} from "reactstrap";
import classnames from "classnames";

class RegisterModal extends Component {
  constructor() {
    super();
    this.state = {
      username: "",
      email: "",
      password: "",
      password2: "",
      errors: {},
    };
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.errors) {
      this.setState({
        errors: nextProps.errors,
      });
    }
  }

  onChange = (e) => {
    this.setState({ [e.target.id]: e.target.value });
  };

  submitRegister = (e) => {
    return new Promise((resolve) => {
      const newUser = {
        username: this.state.username,
        email: this.state.email,
        password: this.state.password,
        password2: this.state.password2,
      };
      this.props.registerUser(newUser, this.props.notify);
      this.props.toggle3(e);
      resolve();
    });
  };

  handleRegister = (e) => {
    e.preventDefault();
    this.submitRegister(e).then(() => {
      this.setState({ username: "", email: "", password: "", password2: "" });
    });
  };

  render() {
    const { errors } = this.state;
    const { isOpen, toggle3, className } = this.props;
    return (
      <Modal isOpen={isOpen} toggle={toggle3} className={className}>
        <ModalHeader toggle={toggle3}>Register</ModalHeader>
        <Form noValidate onSubmit={this.handleRegister}>
          <ModalBody>
            <FormGroup>
              <Label for="username">Username:</Label>
              <Input
                type="text"
                name="username"
                id="username"
                value={this.state.username}
                error={errors.username}
                onChange={this.onChange}
                className={classnames("", {
                  invalid: errors.username,
                })}
              />
            </FormGroup>

            <FormGroup>
              <Label for="email">Email:</Label>
              <Input
                type="text"
                name="email"
                id="email"
                value={this.state.email}
                error={errors.email}
                onChange={this.onChange}
                className={classnames("", {
                  invalid: errors.email,
                })}
              />
            </FormGroup>

            <FormGroup>
              <Label for="password">Password:</Label>
              <Input
                type="password"
                name="password"
                id="password"
                value={this.state.password}
                error={errors.password}
                onChange={this.onChange}
                className={classnames("", {
                  invalid: errors.password,
                })}
              />
            </FormGroup>

            <FormGroup>
              <Label for="password2">Confirm Password:</Label>
              <Input
                type="password"
                name="password2"
                id="password2"
                value={this.state.password2}
                error={errors.password2}
                onChange={this.onChange}
                className={classnames("", {
                  invalid: errors.password2,
                })}
              />
            </FormGroup>
          </ModalBody>
          <ModalFooter>
            <Button color="primary" type="submit">
              Register
            </Button>{" "}
            <Button color="secondary" onClick={toggle3}>
              Cancel
            </Button>
          </ModalFooter>
        </Form>
      </Modal>
    );
  }
}

RegisterModal.propTypes = {
  registerUser: PropTypes.func.isRequired,
  auth: PropTypes.object.isRequired,
  errors: PropTypes.object.isRequired,
};

const mapStateToProps = (state) => ({
  auth: state.auth,
  errors: state.errors,
});

export default connect(mapStateToProps, { registerUser })(RegisterModal);
