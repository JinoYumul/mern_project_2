import React, { Component } from "react";
import {
  Button,
  Modal,
  ModalHeader,
  ModalBody,
  ModalFooter,
  Form,
  FormGroup,
  Label,
  Input,
} from "reactstrap";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import moment from "moment";
import axios from "axios";

class ViewModal extends Component {
  render() {
    const {
      isOpen,
      toggle1,
      className,
      eventID,
      eventName,
      eventGame,
      eventDesc,
      eventStart,
      eventEnd,
      eventRecurs,
      eventSlots,
      eventReserved,
      delRecord,
      updateRecord,
      submitUpdate,
      inputsDisabled,
      onChange,
      recurring,
    } = this.props;

    const { user } = this.props.auth;

    let yesorno;
    if (eventRecurs === null) {
      yesorno = <strong>No</strong>;
    } else {
      yesorno = <strong>Yes</strong>;
    }

    let weeklyInput;
    if (inputsDisabled === true) {
      weeklyInput = <p>Weekly? {yesorno}</p>;
    } else if (eventRecurs === null) {
      weeklyInput = (
        <FormGroup check>
          <Input type="checkbox" onChange={recurring} /> <p>Weekly?</p>
        </FormGroup>
      );
    } else {
      weeklyInput = (
        <FormGroup check>
          <Input type="checkbox" onChange={recurring} checked /> <p>Weekly?</p>
        </FormGroup>
      );
    }

    let dateDisplay1;
    if (inputsDisabled === true) {
      dateDisplay1 = <p>{moment(eventStart).format("MM-DD-YYYY")}</p>;
    } else {
      dateDisplay1 = (
        <Input
          type="date"
          name="start"
          value={moment(eventStart).format("YYYY-MM-DD")}
          onChange={onChange}
        />
      );
    }

    let dateDisplay2;
    if (inputsDisabled === true) {
      dateDisplay2 = <p>{moment(eventEnd).format("MM-DD-YYYY")}</p>;
    } else {
      dateDisplay2 = (
        <Input
          type="date"
          name="end"
          value={moment(eventEnd).format("YYYY-MM-DD")}
          onChange={onChange}
        />
      );
    }

    let slotsLeft = eventSlots - eventReserved.length;
    if (slotsLeft <= 0) {
      slotsLeft = "This event is full";
    }

    let coolBtns;
    if (user.is_admin === true) {
      if (inputsDisabled === true) {
        coolBtns = (
          <React.Fragment>
            {" "}
            <Button color="primary" onClick={updateRecord}>
              Update
            </Button>
            <Button color="danger" onClick={delRecord}>
              Delete
            </Button>
          </React.Fragment>
        );
      } else {
        coolBtns = (
          <React.Fragment>
            {" "}
            <Button color="success" onClick={submitUpdate}>
              Submit
            </Button>
            <Button color="danger" onClick={delRecord}>
              Delete
            </Button>
          </React.Fragment>
        );
      }
    } else if (user.is_admin === false && eventSlots > eventReserved.length) {
      coolBtns = (
        <Button
          color="primary"
          onClick={(e) => {
            e.preventDefault();
            axios.put(`users/add_event/${user.id}`, {
              _id: eventID,
              title: eventName,
              start: eventStart,
              end: eventEnd,
            });
            axios.put(`/api/storeEvents/reserve/${eventID}`, {
              _id: user.id,
            });
            this.props.toggle1(e);
          }}
        >
          Reserve a slot
        </Button>
      );
    }

    return (
      <Modal isOpen={isOpen} toggle={toggle1} className={className}>
        <ModalHeader toggle={toggle1}>Event Details</ModalHeader>
        <Form>
          <ModalBody>
            <FormGroup>
              <Label for="title">Name:</Label>
              <Input
                type="text"
                name="title"
                value={eventName}
                disabled={inputsDisabled}
                onChange={onChange}
              />
            </FormGroup>
            <FormGroup>
              <Label for="game">Game:</Label>
              <Input
                type="text"
                name="game"
                value={eventGame}
                disabled={inputsDisabled}
                onChange={onChange}
              />
            </FormGroup>
            <FormGroup>
              <Label for="desc">Description:</Label>
              <Input
                type="text"
                name="desc"
                value={eventDesc}
                disabled={inputsDisabled}
                onChange={onChange}
              />
            </FormGroup>
            <FormGroup>
              <Label for="start">Start Date:</Label>
              {dateDisplay1}
            </FormGroup>
            <FormGroup>
              <Label for="end">End Date:</Label>
              {dateDisplay2}
            </FormGroup>
            <FormGroup>
              <Label for="slots">Slots Left:</Label>
              <Input
                type="text"
                name="slots"
                value={slotsLeft}
                disabled={inputsDisabled}
                onChange={onChange}
              />
            </FormGroup>
            {weeklyInput}
          </ModalBody>
        </Form>
        <ModalFooter>
          {coolBtns}
          <Button color="secondary" onClick={toggle1}>
            Close
          </Button>
        </ModalFooter>
      </Modal>
    );
  }
}

ViewModal.propTypes = {
  auth: PropTypes.object.isRequired,
};
const mapStateToProps = (state) => ({
  auth: state.auth,
});
export default connect(mapStateToProps)(ViewModal);
