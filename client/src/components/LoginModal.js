import React, { Component } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { loginUser } from "../actions/authActions";
import {
  Button,
  Modal,
  ModalHeader,
  ModalBody,
  ModalFooter,
  Form,
  FormGroup,
  Label,
  Input,
} from "reactstrap";
import classnames from "classnames";

class LoginModal extends Component {
  constructor() {
    super();
    this.state = {
      email: "",
      password: "",
      errors: {},
    };
  }

  componentWillReceiveProps(nextProps) {
    // if (nextProps.auth.isAuthenticated) {
    //   this.props.history.push("/"); // push user to dashboard when they login
    // }
    if (nextProps.errors) {
      this.setState({
        errors: nextProps.errors,
      });
    }
  }

  onChange = (e) => {
    this.setState({ [e.target.id]: e.target.value });
  };

  submitLogin = (e) => {
    return new Promise((resolve) => {
      const userData = {
        email: this.state.email,
        password: this.state.password,
      };
      this.props.loginUser(userData);
      this.props.toggle4(e);
      resolve();
    });
  };

  handleLogin = (e) => {
    e.preventDefault();
    this.submitLogin(e).then(() => {
      this.setState({ email: "", password: "" });
    });
  };

  render() {
    const { errors } = this.state;
    const { isOpen, toggle4, className } = this.props;
    return (
      <Modal isOpen={isOpen} toggle={toggle4} className={className}>
        <ModalHeader toggle={toggle4}>Log In</ModalHeader>
        <Form noValidate onSubmit={this.handleLogin}>
          <ModalBody>
            <FormGroup>
              <Label for="email">Email:</Label>
              <Input
                type="text"
                name="email"
                id="email"
                value={this.state.email}
                error={errors.email}
                onChange={this.onChange}
                className={classnames("", {
                  invalid: errors.email || errors.emailnotfound,
                })}
              />
            </FormGroup>

            <FormGroup>
              <Label for="password">Password:</Label>
              <Input
                type="password"
                name="password"
                id="password"
                value={this.state.password}
                error={errors.password}
                onChange={this.onChange}
                className={classnames("", {
                  invalid: errors.password || errors.passwordincorrect,
                })}
              />
            </FormGroup>
          </ModalBody>
          <ModalFooter>
            <Button color="primary" type="submit">
              Submit
            </Button>{" "}
            <Button color="secondary" onClick={toggle4}>
              Close
            </Button>
          </ModalFooter>
        </Form>
      </Modal>
    );
  }
}

LoginModal.propTypes = {
  loginUser: PropTypes.func.isRequired,
  auth: PropTypes.object.isRequired,
  errors: PropTypes.object.isRequired,
};

const mapStateToProps = (state) => ({
  auth: state.auth,
  errors: state.errors,
});

export default connect(mapStateToProps, { loginUser })(LoginModal);
