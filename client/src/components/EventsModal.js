import React, { Component } from "react";
import {
  Button,
  Modal,
  ModalHeader,
  ModalBody,
  ModalFooter,
  ListGroup,
  ListGroupItem,
  ListGroupItemHeading,
  ListGroupItemText,
} from "reactstrap";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import axios from "axios";
import moment from "moment";

class EventsModal extends Component {
  state = {
    events: [],
  };

  sequence = ["success", "info", "warning", "danger"];

  componentDidUpdate() {
    if (this.props.auth.user.id !== undefined) {
      axios
        .get(`/users/${this.props.auth.user.id}`)
        .then((res) => this.setState({ events: [...res.data.saved_events] }));
    }
  }

  delEvent = (e) => {
    axios.put(`/users/remove_event/${this.props.auth.user.id}`, {
      _id: e.target.parentNode.getAttribute("data-key"),
    });
    axios.put(
      `/api/storeEvents/cancel_reserve/${e.target.parentNode.getAttribute(
        "data-key"
      )}`,
      {
        _id: this.props.auth.user.id,
      }
    );
  };

  render() {
    const myEvents = this.state.events.map((element, i) => {
      if (i === this.sequence.length) {
        i = 0;
      }
      return (
        <ListGroupItem
          className="position-relative"
          color={this.sequence[i]}
          key={element._id}
          data-key={element._id}
        >
          <ListGroupItemHeading> {element.title}</ListGroupItemHeading>
          <ListGroupItemText>
            Starts: {moment(element.start).format("MM-DD-YYYY")}
            <br />
            Ends: {moment(element.end).format("MM-DD-YYYY")}
          </ListGroupItemText>
          <Button
            className="position-absolute"
            size="sm"
            color="danger"
            style={xbuttons}
            onClick={this.delEvent}
          >
            x
          </Button>
        </ListGroupItem>
      );
    });

    const { isOpen, toggle5, className } = this.props;
    return (
      <Modal isOpen={isOpen} toggle={toggle5} className={className}>
        <ModalHeader toggle={toggle5}>My Events</ModalHeader>
        <ModalBody className="p-0">
          {" "}
          <ListGroup>{myEvents} </ListGroup>
        </ModalBody>
        <ModalFooter>
          <Button color="secondary" onClick={toggle5}>
            Close
          </Button>
        </ModalFooter>
      </Modal>
    );
  }
}

const xbuttons = {
  top: "0",
  right: "0",
};

EventsModal.propTypes = {
  auth: PropTypes.object.isRequired,
};
const mapStateToProps = (state) => ({
  auth: state.auth,
});
export default connect(mapStateToProps)(EventsModal);
