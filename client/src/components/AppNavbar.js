import React, { useState } from "react";
import SearchBar from "./SearchBar";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { logoutUser } from "../actions/authActions";
import {
  Collapse,
  Navbar,
  NavbarToggler,
  NavbarBrand,
  Nav,
  NavItem,
  NavLink,
} from "reactstrap";

const AppNavbar = (props) => {
  const { user } = props.auth;

  const [isOpen, setIsOpen] = useState(false);

  const toggleNavbar = () => setIsOpen(!isOpen);

  let leftSide;
  if (user.is_admin === true) {
    leftSide = (
      <Nav className="mr-auto" navbar>
        {" "}
        <NavItem>
          <NavLink href="" onClick={props.toggle2}>
            Add Event
          </NavLink>
        </NavItem>
      </Nav>
    );
  }

  let rightSide;
  if (props.auth.isAuthenticated) {
    rightSide = (
      <Nav className="ml-auto" navbar>
        <NavItem>
          {" "}
          <NavLink href="" onClick={props.toggle5}>
            {user.username}
          </NavLink>
        </NavItem>{" "}
        <NavItem>
          <NavLink
            href=""
            onClick={(e) => {
              e.preventDefault();
              props.logoutUser();
            }}
          >
            Log Out
          </NavLink>
        </NavItem>
      </Nav>
    );
  } else {
    rightSide = (
      <Nav className="ml-auto" navbar>
        {" "}
        <NavItem>
          <NavLink href="" onClick={props.toggle3}>
            Register
          </NavLink>
        </NavItem>
        <NavItem>
          <NavLink href="" onClick={props.toggle4}>
            Log In
          </NavLink>
        </NavItem>
      </Nav>
    );
  }

  return (
    <div>
      <Navbar color="dark" dark expand="md" className="mb-3">
        <NavbarBrand href="">
          <h2 className="ml-5 mb-0">
            <i className="fas fa-book mr-3"></i>AceBook
          </h2>
        </NavbarBrand>
        <NavbarToggler onClick={toggleNavbar} />
        <Collapse isOpen={isOpen} navbar>
          {leftSide}
          <SearchBar />
          {rightSide}
        </Collapse>
      </Navbar>
    </div>
  );
};

AppNavbar.propTypes = {
  logoutUser: PropTypes.func.isRequired,
  auth: PropTypes.object.isRequired,
};
const mapStateToProps = (state) => ({
  auth: state.auth,
});
export default connect(mapStateToProps, { logoutUser })(AppNavbar);
