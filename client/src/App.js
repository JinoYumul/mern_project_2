import React, { Component } from "react";
import jwt_decode from "jwt-decode";
import setAuthToken from "./utils/setAuthToken";
import { setCurrentUser, logoutUser } from "./actions/authActions";
import { Provider } from "react-redux";
import store from "./store";
import AppNavbar from "./components/AppNavbar";
import RegisterModal from "./components/RegisterModal";
import LoginModal from "./components/LoginModal";
import ViewModal from "./components/ViewModal";
import FormModal from "./components/FormModal";
import EventsModal from "./components/EventsModal";
import "react-toastify/dist/ReactToastify.css";
import "react-big-calendar/lib/css/react-big-calendar.css";
import "bootswatch/dist/cerulean/bootstrap.min.css";
import "./App.css";
import { Calendar, momentLocalizer } from "react-big-calendar";
import { ToastContainer, toast } from "react-toastify";
import moment from "moment";
import axios from "axios";

const localizer = momentLocalizer(moment);

// Check for token to keep user logged in
if (localStorage.jwtToken) {
  // Set auth token header auth
  const token = localStorage.jwtToken;
  setAuthToken(token);
  // Decode token and get user info and exp
  const decoded = jwt_decode(token);
  // Set user and isAuthenticated
  store.dispatch(setCurrentUser(decoded));
  // Check for expired token
  const currentTime = Date.now() / 1000; // to get in milliseconds
  if (decoded.exp < currentTime) {
    // Logout user
    store.dispatch(logoutUser());
    // Redirect to login
    // window.location.href = "./login";
  }
}

class App extends Component {
  state = {
    _id: undefined,
    title: "",
    game: "",
    desc: "",
    start: "",
    end: "",
    recursEvery: null,
    slots: 0,
    reservedUsers: [],
    modal1: false,
    modal2: false,
    modal3: false,
    modal4: false,
    modal5: false,
    inputsDisabled: true,
    events: [],
  };

  notify = () =>
    toast.success(
      <div>
        <p className="text-white text-center">
          <strong>{"Registration successful"}</strong>
        </p>
        <p className="text-center">{"You may now log in."}</p>
      </div>,
      {
        position: "top-center",
        autoClose: 4000,
        hideProgressBar: true,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true,
        progress: undefined,
      }
    );

  toggle1 = () => {
    this.setState({
      modal1: !this.state.modal1,
      inputsDisabled: true,
    });
  };

  toggle2 = (e) => {
    e.preventDefault();
    this.setState({
      modal2: !this.state.modal2,
      recursEvery: null,
    });
  };

  toggle3 = (e) => {
    e.preventDefault();
    this.setState({
      modal3: !this.state.modal3,
    });
  };

  toggle4 = (e) => {
    e.preventDefault();
    this.setState({
      modal4: !this.state.modal4,
    });
  };

  toggle5 = (e) => {
    e.preventDefault();
    this.setState({
      modal5: !this.state.modal5,
    });
  };

  componentDidMount() {
    let eventCopy = [];
    axios
      .get("/api/storeEvents")
      .then((res) =>
        res.data.map((event) => {
          if (event.recursEvery !== null) {
            eventCopy.push(event);
            for (let i = 0; i < 52; i++) {
              let x = event.recursEvery;
              eventCopy.push({
                _id: event._id,
                allDay: event.allDay,
                desc: event.desc,
                game: event.game,
                isCanceled: event.isCanceled,
                title: event.title,
                slots: event.slots,
                reservedUsers: event.reservedUsers,
                start: moment(event.start)
                  .add(x * (i + 1), "days")
                  .toDate(),
                end: moment(event.end)
                  .add(x * (i + 1), "days")
                  .toDate(),
              });
            }
          } else {
            eventCopy.push(event);
          }
        })
      )
      .then(() => {
        this.setState({
          events: eventCopy,
        });
      });
  }

  getRecord = (e) => {
    axios.get(`/api/storeEvents/${e._id}`).then((res) =>
      this.setState({
        modal1: !this.state.modal1,
        _id: res.data._id,
        title: res.data.title,
        game: res.data.game,
        desc: res.data.desc,
        start: res.data.start,
        end: res.data.end,
        recursEvery: res.data.recursEvery,
        slots: res.data.slots,
        reservedUsers: res.data.reservedUsers,
      })
    );
  };

  updateRecord = () => {
    this.setState({
      inputsDisabled: false,
    });
  };

  submitUpdate = (e) => {
    let eventCopy = [];
    e.preventDefault();
    axios
      .put(`/api/storeEvents/update/${this.state._id}`, {
        title: this.state.title,
        game: this.state.game,
        desc: this.state.desc,
        start: this.state.start,
        end: this.state.end,
        recursEvery: this.state.recursEvery,
        slots: this.state.slots,
      })
      .then((res) => {
        if (res.data.recursEvery !== null) {
          eventCopy.push(res.data);
          for (let i = 0; i < 52; i++) {
            let x = res.data.recursEvery;
            eventCopy.push({
              _id: res.data._id,
              allDay: res.data.allDay,
              desc: res.data.desc,
              game: res.data.game,
              isCanceled: res.data.isCanceled,
              reservedUsers: res.data.reservedUsers,
              title: res.data.title,
              start: moment(res.data.start)
                .add(x * (i + 1), "days")
                .toDate(),
              end: moment(res.data.end)
                .add(x * (i + 1), "days")
                .toDate(),
            });
          }
        } else {
          eventCopy.push(res.data);
        }
      })
      .then((res) =>
        this.setState({
          title: "",
          game: "",
          desc: "",
          start: "",
          end: "",
          recursEvery: null,
          slots: 0,
          reservedUsers: [],
          modal1: !this.state.modal1,
          inputsDisabled: true,
          events: [
            ...this.state.events.filter(
              (event) => event._id !== this.state._id
            ),
            ...eventCopy,
          ],
        })
      );
  };

  delRecord = () => {
    axios.delete(`/api/storeEvents/${this.state._id}`).then((res) =>
      this.setState({
        modal1: !this.state.modal1,
        events: [
          ...this.state.events.filter((event) => event._id !== this.state._id),
        ],
      })
    );
  };

  onChange = (e) => {
    this.setState({
      [e.target.name]: e.target.value,
    });
  };

  recurring = () => {
    if (this.state.recursEvery === null) {
      this.setState({
        recursEvery: 7,
      });
    } else if (this.state.recursEvery === 7) {
      this.setState({
        recursEvery: null,
      });
    }
  };

  postRecord = (e) => {
    let eventCopy = [];
    e.preventDefault();
    axios
      .post("/api/storeEvents", {
        title: this.state.title,
        game: this.state.game,
        desc: this.state.desc,
        start: this.state.start,
        end: this.state.end,
        recursEvery: this.state.recursEvery,
        slots: this.state.slots,
      })
      .then((res) => {
        if (res.data.recursEvery !== null) {
          eventCopy.push(res.data);
          for (let i = 0; i < 52; i++) {
            let x = res.data.recursEvery;
            eventCopy.push({
              _id: res.data._id,
              allDay: res.data.allDay,
              desc: res.data.desc,
              game: res.data.game,
              isCanceled: res.data.isCanceled,
              reservedUsers: res.data.reservedUsers,
              title: res.data.title,
              start: moment(res.data.start)
                .add(x * (i + 1), "days")
                .toDate(),
              end: moment(res.data.end)
                .add(x * (i + 1), "days")
                .toDate(),
            });
          }
        } else {
          eventCopy.push(res.data);
        }
      })
      .then((res) =>
        this.setState({
          title: "",
          game: "",
          desc: "",
          start: "",
          end: "",
          recursEvery: null,
          slots: 0,
          reservedUsers: [],
          modal2: !this.state.modal2,
          events: [...this.state.events, ...eventCopy],
        })
      );
  };

  render() {
    return (
      <Provider store={store}>
        <div style={calContainer}>
          <ToastContainer />
          <AppNavbar
            addEvent={this.addEvent}
            toggle2={this.toggle2.bind(this)}
            toggle3={this.toggle3.bind(this)}
            toggle4={this.toggle4.bind(this)}
            toggle5={this.toggle5.bind(this)}
          />
          <RegisterModal
            isOpen={this.state.modal3}
            toggle3={this.toggle3.bind(this)}
            notify={this.notify.bind(this)}
          />
          <LoginModal
            isOpen={this.state.modal4}
            toggle4={this.toggle4.bind(this)}
          />
          <Calendar
            selectable
            localizer={localizer}
            events={this.state.events}
            views={{ month: true }}
            onSelectEvent={this.getRecord.bind(this)}
            startAccessor="start"
            endAccessor="end"
          />
          <ViewModal
            isOpen={this.state.modal1}
            toggle1={this.toggle1.bind(this)}
            eventID={this.state._id}
            eventName={this.state.title}
            eventGame={this.state.game}
            eventDesc={this.state.desc}
            eventStart={this.state.start}
            eventEnd={this.state.end}
            eventRecurs={this.state.recursEvery}
            eventSlots={this.state.slots}
            eventReserved={this.state.reservedUsers}
            delRecord={this.delRecord.bind(this)}
            updateRecord={this.updateRecord.bind(this)}
            submitUpdate={this.submitUpdate.bind(this)}
            inputsDisabled={this.state.inputsDisabled}
            onChange={this.onChange.bind(this)}
            recurring={this.recurring.bind(this)}
          />
          <FormModal
            isOpen={this.state.modal2}
            toggle2={this.toggle2.bind(this)}
            postRecord={this.postRecord.bind(this)}
            onChange={this.onChange.bind(this)}
            recurring={this.recurring.bind(this)}
          />
          <EventsModal
            isOpen={this.state.modal5}
            toggle5={this.toggle5.bind(this)}
            getRecord={this.getRecord.bind(this)}
          />
        </div>
      </Provider>
    );
  }
}

const calContainer = {
  height: "90vh",
};

export default App;
